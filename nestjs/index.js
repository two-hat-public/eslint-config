/**
 * NestJS Lint Rules
 *
 * Currently just a placeholder, but can be extended with rules specific to NestJS projects.
 */
module.exports = {
  
  // Inherits general TypeScript config, which itself inherits from base JavaScript
  extends: [ '../typescript' ]
  
};