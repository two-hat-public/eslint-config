# [Release 2.2.0](https://gitlab.com/two-hat-public/eslint-config/compare/v2.1.1...v2.2.0)

### Features

* WIP add rules for modtools ([c3efe3b](https://gitlab.com/two-hat-public/eslint-config/commit/c3efe3b87cf1336953ccb3f440a83dd63061bc3d))

## [Release 2.1.1](https://gitlab.com/two-hat-public/eslint-config/compare/v2.1.0...v2.1.1)

### Bug Fixes

* **deps:** update typescript-eslint monorepo to v4 ([59f05a2](https://gitlab.com/two-hat-public/eslint-config/commit/59f05a2a5b3abdf30746bf712997b8a3d3535165))

# [Release 2.1.0](https://gitlab.com/two-hat-public/eslint-config/compare/v2.0.1...v2.1.0)

### Bug Fixes

* add "r" to identifier blacklist ([589d92d](https://gitlab.com/two-hat-public/eslint-config/commit/589d92d9ad6eb404dacac8a1201a5a270ecb0a82))


### Features

* **id-blacklist:** blacklist some identifiers ([90b60bd](https://gitlab.com/two-hat-public/eslint-config/commit/90b60bd62176b4a8fb8fa61a9715c00b4ac5b3a1))

## [Release 2.0.1](https://gitlab.com/two-hat-public/eslint-config/compare/v2.0.0...v2.0.1)

### Bug Fixes

* **deps:** update deps to close vulns ([820468a](https://gitlab.com/two-hat-public/eslint-config/commit/820468a86049031e4462e6a1400fa9c8a361cf63))

# [Release 2.0.0](https://gitlab.com/two-hat-public/eslint-config/compare/v1.0.1...v2.0.0)

### Features

* split into multiple-config monorepo ([7faa644](https://gitlab.com/two-hat-public/eslint-config/commit/7faa6443c1d6f3edc968a96550435a1754510780))


### BREAKING CHANGES

* ESLint configs in dependent projects will need to be updated to refer to the proper path.
See README.md for usage examples.

## [Release 1.0.1](https://gitlab.com/two-hat-public/eslint-config/compare/v1.0.0...v1.0.1)

### Bug Fixes

* **eslint:** fix bad ignorePatterns ([dcb22cb](https://gitlab.com/two-hat-public/eslint-config/commit/dcb22cb50c62e1bd010c07b68137e2d9306c0748))

# [Release 1.0.0](https://gitlab.com/two-hat-public/eslint-config/compare/v0.0.8...v1.0.0)

### Features

* migrate to typescript-based linting ([56db55a](https://gitlab.com/two-hat-public/eslint-config/commit/56db55a33963b6e1802a4809469f2c34d0d3d25e))


### BREAKING CHANGES

* Will probably cause issues with non-typescript projects.
Upgrade with caution.
