/**
 * Base ESLint config for TypeScript
 * See subdirectories for more specific linting rules
 *
 * Usage example for an Angular project:
 * (.eslintrc.js)
 * {
 *   "extends": [ "@twohatsecurity/eslint-config/angular" ]
 * }
 *
 * We are using the .JS version of an ESLint config file here so that we can
 * add lots of comments to better explain and document the setup.
 */
module.exports = {

  // Inherits base JavaScript rules
  extends: [ '..' ],

  env: {
    browser: true,
    node: true,
    es2020: true,
  },

  overrides: [
    {
      files: [ '*.ts' ],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module'
      },

      plugins: [
        '@typescript-eslint',
        'simple-import-sort',
        'import'
      ],

      extends: [
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'prettier',
        'prettier/@typescript-eslint',
      ],

      rules: {

        '@typescript-eslint/explicit-function-return-type': 'off',

        '@typescript-eslint/explicit-member-accessibility': 'off',

        '@typescript-eslint/interface-name-prefix': 'off',

        '@typescript-eslint/member-ordering': [
          'error',
          {
            default: [
              'static-field',
              'instance-field',
              'static-method',
              'instance-method'
            ]
          }
        ],
        
        '@typescript-eslint/no-explicit-any': 'off',
        
        'no-restricted-imports': [
          'error',
          {
            paths: [
              { name: 'rxjs/Rx', message: "Please import directly from 'rxjs' instead" }
            ]
          }
        ],

        'no-restricted-syntax': [
          'error',
          {
            selector: 'CallExpression[callee.object.name="console"][callee.property.name=/^(debug|info|time|timeEnd|trace)$/]',
            message: 'Unexpected property on console object was called'
          }
        ],
        
        'simple-import-sort/sort': 'warn'

      }
    }
  ]
};
