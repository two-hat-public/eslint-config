# Two Hat Security Common ESLint Config

## Inheritance Tree

```text
Base JavaScript
└── Base TypeScript
  │   ├── Angular
  │   └── NestJS
  └── NodeJS
```

## Using in other projects

In your project, run `npm install --save-dev eslint @twohatsecurity/eslint-config` to install ESLint and this repo to your devDependencies.

Create a file in the project root named `.eslintrc.js`.

Depending on the type of project, add the following to the contents:

### Base JavaScript

The base ruleset from which everything else inherits.

```javascript
module.exports = {
  extends: ["@twohatsecurity/eslint-config"],
};
```

### Base TypeScript

Inherits from Base JavaScript

```javascript
module.exports = {
  extends: ["@twohatsecurity/eslint-config/typescript"],
};
```

### Angular

Contains Angular-specific config, and inherits from generic TypeScript

```javascript
module.exports = {
  extends: ["@twohatsecurity/eslint-config/angular"],
};
```

### NestJS

Placeholder config; inherits from generic TypeScript

```javascript
module.exports = {
  extends: ["@twohatsecurity/eslint-config/nestjs"],
};
```

### NodeJS (vanilla JavaScript)

Rules specific to NodeJS.

```javascript
module.exports = {
  extends: ["@twohatsecurity/eslint-config/nodejs"],
};
```
