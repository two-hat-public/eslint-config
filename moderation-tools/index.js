/**
 * JavaScript rules for Moderation Tools ("Bernie")
 * Restricts JS to ES5 features, but we hope to have this limitation removed soon.
 */

module.exports = {

  'extends': [
    'eslint:recommended'
  ],

  'parserOptions': {
    'ecmaVersion': 5
  },

  'env': {
    'browser': true
  },

  'plugins': [],

  'rules': {
    'accessor-pairs': ['warn', { 'setWithoutGet': true }],
    'block-scoped-var': 'warn',
    'callback-return': 'error',
    'default-case': 'warn',
    'default-param-last': 'warn',
    'dot-notation': 'warn',
    'eqeqeq': ['warn', 'smart'],
    // SL: I have a feeling this one would be incredibly noisy
    //'func-style': ['warn', 'declaration', { 'allowArrowFunctions': false }],
    'guard-for-in': 'warn',
    'id-blacklist': ['error', 'cb', 'e', 'r', 'x', 'z'],
    'new-cap': ['warn', { 'capIsNew': true }],
    'no-cond-assign': ['error', 'except-parens'],
    'no-console': 'warn',
    'no-debugger': 'error',
    'no-else-return': 'warn',
    'no-eval': 'error',
    'no-extend-native': 'warn',
    'no-extra-bind': 'warn',
    'no-implied-eval': 'warn',
    'no-labels': 'error',
    'no-lone-blocks': 'warn',
    'no-lonely-if': 'warn',
    'no-loop-func': 'warn',
    'no-multi-str': 'error',
    'no-obj-calls': 'error',
    'no-return-await': 'error',
    'no-unneeded-ternary': 'warn',
    'no-useless-concat': 'error',
    'no-useless-return': 'warn',
    'operator-assignment': ['warn', 'always'],
    'prefer-promise-reject-errors': 'warn',
    'sort-vars': ['warn', { 'ignoreCase': true }],
    'spaced-comment': ['error', 'always', { 'line': { 'exceptions': ['TODO', 'HACK', 'DEBUG', '///', '**', '***'] } }],
    'yoda': 'warn'
  }
};
