/**
 * NodeJS Lint Rules
 */
module.exports = {

  // Inherits base JavaScript rules
  extends: ['..'],

  rules: {
    // Conflicts with simple-import-sort
    'import/group-exports': 'warn',
    'import/order': ['warn', { 'alphabetize': { 'order': 'asc' }, 'newlines-between': 'always' }],
    'import/newline-after-import': 2,
    'sort-imports': [1, { 'ignoreCase': true }]
  }

};