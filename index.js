/**
 * Base JavaScript rules
 */

module.exports = {

  'extends': [
    'eslint:recommended',
    'prettier'
  ],

  'parserOptions': {
    'ecmaVersion': 2019
  },

  'env': {
    'node': true
  },

  'plugins': [
    'import'
  ],

  'rules': {
    'accessor-pairs': ['warn', { 'setWithoutGet': true }],
    'block-scoped-var': 'warn',
    'callback-return': 'error',
    'constructor-super': 'warn',
    'default-case': 'warn',
    'default-param-last': 'warn',
    'dot-notation': 'warn',
    'eqeqeq': ['error', 'smart'],
    'func-style': ['error', 'declaration', { 'allowArrowFunctions': true }],
    'guard-for-in': 'warn',
    'handle-callback-err': 'warn',
    'id-blacklist': ['error', 'cb', 'e', 'r', 'x', 'z'],
    'import/first': 'warn',
    'import/no-absolute-path': 'error',
    'import/no-cycle': 'warn',
    'import/no-duplicates': 'error',
    'import/no-dynamic-require': 'error',
    'import/no-self-import': 'warn',
    'import/no-useless-path-segments': 'warn',
    'new-cap': ['off', { 'capIsNew': true }],
    'no-class-assign': 'error',
    'no-cond-assign': ['error', 'except-parens'],
    'no-console': 'warn',
    'no-const-assign': 'error',
    'no-constant-condition': 'error',
    'no-control-regex': 'error',
    'no-debugger': 'error',
    'no-dupe-args': 'error',
    'no-dupe-keys': 'error',
    'no-duplicate-case': 'error',
    'no-else-return': 'warn',
    'no-empty-character-class': 'error',
    'no-empty-pattern': 'warn',
    'no-empty': 'warn',
    'no-eq-null': 'error',
    'no-eval': 'error',
    'no-ex-assign': 'error',
    'no-extend-native': 'warn',
    'no-extra-bind': 'warn',
    'no-extra-boolean-cast': 'warn',
    'no-fallthrough': 'warn',
    'no-func-assign': 'warn',
    'no-implied-eval': 'warn',
    'no-invalid-regexp': 'error',
    'no-labels': 'error',
    'no-lone-blocks': 'warn',
    'no-lonely-if': 'error',
    'no-loop-func': 'warn',
    'no-multi-str': 'error',
    'no-native-reassign': 'error',
    'no-negated-in-lhs': 'error',
    'no-obj-calls': 'error',
    'no-regex-spaces': 'warn',
    'no-return-await': 'error',
    'no-sparse-arrays': 'warn',
    'no-this-before-super': 'error',
    'no-unneeded-ternary': 'warn',
    'no-unreachable': 'warn',
    'no-unused-vars': ['error', { 'args': 'after-used' }],
    'no-useless-concat': 'error',
    'no-useless-return': 'warn',
    'no-var': 'error',
    'object-shorthand': ['warn', 'always'],
    'operator-assignment': ['warn', 'always'],
    'prefer-const': 'warn',
    'prefer-promise-reject-errors': 'warn',
    'prefer-spread': 'warn',
    'prefer-template': 'error',
    'require-await': 'off',
    'sort-vars': ['warn', { 'ignoreCase': true }],
    'spaced-comment': ['error', 'always', { 'line': { 'exceptions': ['TODO', 'HACK', 'DEBUG', '///', '**', '***'] } }],
    'use-isnan': 'error',
    'valid-typeof': 'warn',
    'yoda': 'warn'
  }
};
