/**
 * Angular ESLint Config
 * Inherits base config from parent
 */

const allowedPrefixes = [
  // Shared components/directives
  'mod',
  // Main module
  'main',
  // Filter quality
  'ftq',
];

/**
 * We are using the .JS version of an ESLint config file here so that we can
 * add lots of comments to better explain and document the setup.
 */
module.exports = {

  // Inherits general TypeScript config, which itself inherits from base JavaScript
  extends: [ '../typescript' ],

  env: {
    browser: true,
    node: true,
    es2020: true
  },

  overrides: [
    {
      files: [ '*.ts' ],
      parser: '@typescript-eslint/parser',
      
      parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
        project: './tsconfig.json',
      },

      plugins: [
        '@typescript-eslint',
        '@angular-eslint'
      ],

      extends: [
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'prettier',
        'prettier/@typescript-eslint',
      ],

      rules: {

        '@angular-eslint/component-class-suffix': 'error',
        
        '@angular-eslint/component-selector': [
          'error',
          {
            type: 'element',
            prefix: allowedPrefixes,
            style: 'kebab-case'
          },
        ],

        '@angular-eslint/contextual-lifecycle': 'error',

        '@angular-eslint/directive-class-suffix': 'error',

        '@angular-eslint/directive-selector': [
          'error',
          {
            type: 'attribute',
            prefix: allowedPrefixes,
            style: 'camelCase'
          },
        ],

        '@angular-eslint/no-conflicting-lifecycle': 'error',

        '@angular-eslint/no-host-metadata-property': 'error',

        '@angular-eslint/no-input-rename': 'error',
        
        '@angular-eslint/no-inputs-metadata-property': 'error',

        '@angular-eslint/no-output-native': 'error',

        '@angular-eslint/no-output-on-prefix': 'error',

        '@angular-eslint/no-output-rename': 'error',

        '@angular-eslint/no-outputs-metadata-property': 'error',
        
        '@angular-eslint/use-lifecycle-interface': 'warn',
        
        '@angular-eslint/use-pipe-transform-interface': 'error',

        // ORIGINAL tslint.json -> "template-banana-in-box": true,
        // APPLIED VIA TEMPLATE-RELATED CONFIG BELOW

        // ORIGINAL tslint.json -> "template-no-negated-async": true,
        // APPLIED VIA TEMPLATE-RELATED CONFIG BELOW

        // ORIGINAL tslint.json -> "deprecation": { "severity": "warning" },
        /**
         * | [`deprecation`]              | 🌓  | [`import/no-deprecated`] <sup>[1]</sup>            |
         * <sup>[1]</sup> Only warns when importing deprecated symbols<br>
         */

      },
    },
    {
      files: [ '*.component.html' ],
      parser: '@angular-eslint/template-parser',
      plugins: [ '@angular-eslint/template' ],
      rules: {
        // ORIGINAL tslint.json -> "template-banana-in-box": true,
        '@angular-eslint/template/banana-in-a-box': 'error',

        // ORIGINAL tslint.json -> "template-no-negated-async": true,
        '@angular-eslint/template/no-negated-async': 'error',
      },
    },
    {
      files: [ '*.component.ts' ],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
      },
      plugins: [ '@angular-eslint/template' ],
      processor: '@angular-eslint/template/extract-inline-html'
    },
  ],
};
